import React, {useEffect} from "react";
import Home from "./pages/Home";
import "./styles/App.scss";

import { gapi } from "gapi-script";
import { CLIENT_ID } from "./utils/constants";


export default function App(){

    useEffect(()=>{
        function start(){
            gapi.client.init({
                client: CLIENT_ID,
                scope: ""
            })
        }
        gapi.load("client:auth2", start);
    },[]);

    return (
        <Home/>
    )
};
