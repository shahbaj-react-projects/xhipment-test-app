import React,{useEffect, useState} from 'react'
import axios from 'axios';

import Header from '../components/Header'
import PostListing from '../components/PostListing';
import styled from 'styled-components'
import CreatePost from '../components/CreatePost';

import { Route, Routes, Navigate} from "react-router-dom"


const Content = styled.div`
margin:0 auto;
margin-top: 110px;
padding-top: 10px;
padding-bottom: 80px;
max-height: calc(100vh - 110px);
overflow-y: scroll;
`;

export default function Home() {

  const [isLogin, setIsLogin] = useState(false);
  const [currUser, setCurrUser] = useState(null);
  const [posts, setPosts] = useState(null);

  useEffect(() => {
    
    (async function(){
      const response = await axios.get("https://jsonplaceholder.typicode.com/posts");
      console.log("response: ", response);
      if(response.status === 200) setPosts(response.data)
    })();

  },[]);

  return (
    <>
      <Header isLogin={isLogin} setIsLogin={setIsLogin} currUser={currUser} setCurrUser={setCurrUser}/> 
    <Content>
      <Routes>
        <Route path="/" element={ <PostListing isLoggedIn={isLogin} posts={posts} setPosts={setPosts}/>} />
        {<Route path="/create-post" element={isLogin ? <CreatePost currUser={currUser} posts={posts} setPosts={setPosts}/>: <Navigate to="/" replace />} />}
      </Routes>
    </Content>
    </>
  )
}
