import React from 'react'
import axios from 'axios';
import styled from 'styled-components';
import SinglePost from './SinglePost';

const Container = styled.div`
display: flex;
justify-content: center;
flex-wrap: wrap;
gap: 24px;
`;


export default function PostListing({posts, setPosts, isLoggedIn}) {

  const deletePostHandler = (id) => {
    const newPosts = posts.filter((post) => post.id !== id);
    setPosts(newPosts);
    axios.delete(`https://jsonplaceholder.typicode.com/posts/${id}`)
    .then((res)=> console.log("Delete request sent!"))
    .catch((err) => console.log(err))
  }

  const editPostHandler = (id, changes) => {
    const newPosts = posts.map((post) => {
      if (post.id === id) {
          return {
            ...post,
            ...changes
          }
      }else return post;
    });
    setPosts(newPosts);
    axios.put(`https://jsonplaceholder.typicode.com/posts/${id}`, {
      ...changes
    }).then((res)=> console.log("Edit request sent!"))
    .catch((err) => console.log(err))
  }

  return (
    <Container>
      {posts && posts.map((post) => <SinglePost key={post.id} {...post} deletePostHandler={deletePostHandler} editPostHandler={editPostHandler}
        isLoggedIn={isLoggedIn}
      />)}
    </Container>
  )
}
