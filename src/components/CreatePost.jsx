import axios from 'axios';
import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom';

import styled from 'styled-components'

const Container = styled.div`
display: flex;
justify-content: center;
align-items: center;
flex-direction: column;
box-shadow: rgba(17, 12, 46, 0.15) 0px 48px 100px 0px;
margin: 0 auto;
width: fit-content;
padding: 0 48px;
`;

const Label = styled.label``;

const CreateButton = styled.button`
margin-bottom: 32px;
padding: 10px 16px;
border: none;
text-align: center;
`;

export default function CreatePost({posts, setPosts, currUser}) {

  const [userId, setUserId] = useState(currUser ? currUser.email : '');
  const [title, setTitle] = useState("");
  const [body, setBody] = useState("");

  const navigate = useNavigate();
  const postId = Array.isArray(posts)  ? posts.length + 1: 1;

  const createPostHandler = () => {

    if(userId?.trim()==="" || title?.trim()==="" || body?.trim()===""){
      alert("Please fill all fields!");
      return;
    }
  

    axios.post("https://jsonplaceholder.typicode.com/posts", {
        userId,
        title,
        body
      }).then(() => {
        alert("Post Created");
        const newPosts = [...posts, {id: postId, userId, title, body}];
        setPosts(newPosts);
        navigate("/");
      })
      .catch(err => alert(err))
  }


  return (
    <Container>
      <h2>-:CreatePost:-</h2>
      <table>
        <tbody>
          <tr>
            <td><Label htmlFor='postId'>ID: </Label></td>
            <td><input id="postId" disabled value={postId}></input></td>
          </tr>
          <tr>
            <td><Label htmlFor='userId'>UserId: </Label></td>
            <td><input id="userId" value={userId} onChange={(e) => setUserId(e.target.value)}></input></td>
          </tr>
          <tr>
            <td><Label htmlFor='title'>Title: </Label></td>
            <td><input id="title" onChange={(e) => setTitle(e.target.value)}></input></td>
          </tr>
          <tr>
            <td><Label htmlFor='body'>Body: </Label></td>
            <td><textarea id="body" onChange={(e) => setBody(e.target.value)} rows={4}></textarea></td>
          </tr>
        </tbody>
      </table>
      <CreateButton onClick={createPostHandler}>Create</CreateButton>
    </Container>
  )
}