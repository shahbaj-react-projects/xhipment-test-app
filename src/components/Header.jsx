import React,{useState} from 'react';
import  {GoogleLogin, GoogleLogout } from "react-google-login";
import styled from 'styled-components';
import { Link } from 'react-router-dom';

import {CLIENT_ID} from "./../utils/constants";

const Container = styled.div`
position: fixed;
top: 0px;
left: 0;
z-index: 1000;
display: flex;
align-items:center;
justify-content: flex-end;
height:80px;
padding: 0 32px;
width: 100%;
background-color: darkgray;
overflow: hidden;
`;

const UserName = styled.p`
margin-left: auto;
margin-right: 32px;
color: #fff;
font-weight: 600;
text-decoration: underline;
`;

const Button = styled.button`
padding: 10px;
color: rgba(0, 0, 0, 0.54);
font-size: 14px;
font-weight: 500;
background-color: #fff;
margin-right: 12px;
cursor: pointer;
border: none;
`;

export default function Header({isLogin, setIsLogin, currUser, setCurrUser}) {


  const onLoginSuccess = (response) => {
    setIsLogin(true);
    setCurrUser(response.profileObj);
    console.log(response);
  }

  const onLoginFailure = (response) => {
      console.log("Login Failed", response);
  }

  const logoutSuccessHandler = () => {
    // console.log("Logout Success");
    setIsLogin(false);
  }

  const logoutFailureHandler = () => {
    console.log("Logout: Failed");
  }

  return (
    <Container>
    <Link to="/"><Button>Home</Button></Link>
      { isLogin ?  (
        <>
          <Link to="/create-post"><Button>Create a Post</Button></Link>
          { currUser?.name && <UserName>{currUser?.name}</UserName> }
          <GoogleLogout
            clientId={CLIENT_ID}
            buttonText="Logout"
            onLogoutSuccess={logoutSuccessHandler}
            onFailure={logoutFailureHandler}
            icon={false}
            isSignedIn={true}
          />
        </>
      ) : (
        <GoogleLogin
          clientId = {CLIENT_ID}
          buttonText = "Login"
          onSuccess = {onLoginSuccess}
          onFailure = {onLoginFailure}
          cookiePolicy = {'single_host_origin'} 
          icon={false}
          isSignedIn={true}
        />
      )}
    </Container>
  )
}
