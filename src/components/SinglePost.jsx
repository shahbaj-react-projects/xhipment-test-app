import React, { useState, useRef } from 'react'
import styled, { css } from 'styled-components';

const PostContainer = styled.div`
padding: 0 32px;
width: 252px;box-shadow: rgba(17, 12, 46, 0.15) 0px 48px 100px 0px;
display: flex;
flex-direction: column;
justify-content: space-between`
;

const ActionContainer = styled.div`
margin-bottom: 18px;
margin-top: auto;
`;

const DeletePostButton = styled.button`
border: none;
outline: none;
padding: 8px 12px;
margin-left: 18px;
`;


const EditPostButton = styled.button`
border: none;
outline: none;
padding: 8px 12px;
`;
const SavePostButton = styled(EditPostButton)`
`;

const Serial = styled.p`
display: inline-block;
`;

const Title = styled.p`font-weight: 600;
${props => props.editable && css`
  -webkit-user-modify: read-write;
  outline: auto;
`}`;

const Description = styled.p`
${props => props.editable && css`
  -webkit-user-modify: read-write;
  outline: auto;
`}
`;

export default function SinglePost({isLoggedIn, id, title, body, deletePostHandler, editPostHandler}) {

  const [editable, setEditable] = useState(false);
  const titleRef = useRef();
  const bodyRef = useRef();


  const toggleEdit = () => setEditable((prev) => !prev);

  const onSaveClick = (id) => {
    // console.log("Save: ", id, titleRef?.current.innerText, bodyRef?.current.innerText);
    editPostHandler(id, {
      title: titleRef.current.innerText,
      body: bodyRef.current.innerText
    })
    toggleEdit();
  }

  return (
    <PostContainer>
      <Serial>{id}.</Serial>
      { title && <Title editable={editable} ref={titleRef}>{title}</Title>}
      {body && <Description editable={editable} onChange={(e)=>console.log(e.target.value)} ref={bodyRef}>{body}</Description>}
     { isLoggedIn && <ActionContainer>
        {editable && <SavePostButton onClick={() => onSaveClick(id)}>Save</SavePostButton>}
        {!editable && <EditPostButton onClick={() => toggleEdit()}>Edit</EditPostButton>}
        <DeletePostButton onClick={() => deletePostHandler(id)}>Delete</DeletePostButton>
      </ActionContainer>}
    </PostContainer>
  )
}