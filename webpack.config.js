const HtmlWebPackPlugin = require("html-webpack-plugin");
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const TerserPlugin = require("terser-webpack-plugin");
const path = require("path");
require("@babel/polyfill");

module.exports = {
  entry: ["@babel/polyfill", path.resolve(__dirname, "./src/index.js")],
  output: {
    path: path.resolve(__dirname, "build"),
    filename: "[name].[contenthash:8].bundle.js",
    clean:true,
    assetModuleFilename:'[name][ext]',
    publicPath: "/",
  },
  module: {
    rules: [
      {
        test: /\.svg$/,
        use: "svg-inline-loader",
      },
      {
        test: /\.scss$/,
        use: ["style-loader", "css-loader", "sass-loader"],
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
        },
        resolve: {
          extensions: [".js", ".jsx"],
        },
      },
    ],
  },
  devtool:'source-map',
  devServer: {
    static:{
      directory: path.resolve(__dirname, "build")
    },
    port:8080,
    open:true,
    hot:true,
    compress:true,
    historyApiFallback: true,
  },
  plugins: [
    new HtmlWebPackPlugin({ template: "./src/index.html" }),
    // new BundleAnalyzerPlugin()
  ],
  optimization: {
    minimize: true,
    minimizer: [new TerserPlugin()],
  },
};
